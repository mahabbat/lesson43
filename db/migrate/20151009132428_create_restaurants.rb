class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end

    add_attachment :restaurants, :picture
  end
end
